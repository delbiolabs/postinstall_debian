mkfile_path := $(abspath $(lastword $(MAKEFILE_LIST)))
dir_path := $(dir $(mkfile_path))

install:
	@echo installing executables to ${HOME}/.local/bin
	@mkdir -p ${HOME}/.local/bin
	@ln -s ${dir_path}wizard.sh ${HOME}/.local/bin
	@ln -s ${dir_path}progs_installer ${HOME}/.local/bin

uninstall:
	@echo removing executables to ${HOME}/.local/bin
	@rm -f ${HOME}/.local/bin/wizard.sh
	@rm -f ${HOME}/.local/bin/progs_installer

.PHONY: install uninstall

